import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { RouterModule, Routes } from '@angular/router';

import { NavbarComponent } from './components/navbar/navbar.component';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';

import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';


// Customer Views
import { HomeComponent as CustomerHome } from './views/customer/home/home.component';
import { InsertComponent as CustomerInsert } from './views/customer/insert/insert.component';
import { UpdateComponent as CustomerUpdate } from './views/customer/update/update.component';

// Product Views
import { HomeComponent as ProductHome } from './views/product/home/home.component';
import { InsertComponent as ProductInsert } from './views/product/insert/insert.component';
import { UpdateComponent as ProductUpdate } from './views/product/update/update.component';

// Selling Views
import { HomeComponent as SellingHome } from './views/selling/home/home.component';
import { CreateComponent as SellingInsert } from './views/selling/create/create.component';
import { UpdateComponent as SellingUpdate } from './views/selling/update/update.component';

// Index
import { IndexComponent } from './views/index/index.component';

const routes: Routes = [
  { path: 'home', component: IndexComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },

  { path: 'customers', component: CustomerHome },
  { path: 'customers/new', component: CustomerInsert },
  { path: 'customers/update/:id_customer', component: CustomerUpdate },

  { path: 'products', component: ProductHome },
  { path: 'products/new', component: ProductInsert },
  { path: 'products/update/:id_product', component: ProductUpdate },

  { path: 'sellings', component: SellingHome },
  { path: 'sellings/new', component: SellingInsert },
  { path: 'sellings/update/:id_selling', component: SellingUpdate },
];

@NgModule({
  declarations: [
    // General
    AppComponent,
    NavbarComponent,

    // Index View
    IndexComponent,

    // Customer Views
    CustomerHome,
    CustomerInsert,
    CustomerUpdate,

    //Product Views
    ProductHome,
    ProductInsert,
    ProductUpdate,

    //Selling Views
    SellingHome,
    SellingInsert,
    SellingUpdate,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    HttpClientModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatGridListModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatExpansionModule,
    MatListModule,
    MatDialogModule,
    MatMenuModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
