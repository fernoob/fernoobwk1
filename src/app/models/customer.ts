export interface Customer {
  id_customer?: number;
  customer?: string;
  cid?: string;
  cpf?: string;
  birthday?: string;
  email?: string;
  address?: string;
  address2?: string;
  cep?: string;
  number?: string;
  city?: string;
}
