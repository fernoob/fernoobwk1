export interface Product {
  id_product?: number;
  pid?:string;
  product?: string;
  price?: number;
}
