export interface Selling {
  id_selling?: number,
  id_customer?: number,
  date?: string,
  products?: Array<number>,
}
