import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Selling } from '../models/selling';

@Injectable({
  providedIn: 'root'
})
export class SellingService {

  constructor(private httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  url = 'http://fernoob1wk1.gq/api/selling';

  getSellings(): Observable<Selling[]> {
    return this.httpClient.get<Selling[]>(this.url)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  // Obtem uma compra pelo id
  getSellingById(id: number): Observable<Selling> {
    return this.httpClient.get<Selling>(this.url + '/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // salva uma compra
  saveSelling(selling: Selling): Observable<Selling> {
    return this.httpClient.post<Selling>(this.url, JSON.stringify(selling), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // atualiza uma compra
  updateSelling(selling: Selling): Observable<Selling> {
    return this.httpClient.put<Selling>(this.url + '/' + selling.id_selling, JSON.stringify(selling), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // deleta uma compra
  deleteSelling(selling: Selling) {
    return this.httpClient.delete<Selling>(this.url + '/' + selling.id_selling, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
