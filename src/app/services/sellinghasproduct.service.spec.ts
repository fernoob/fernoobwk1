import { TestBed } from '@angular/core/testing';

import { SellinghasproductService } from './sellinghasproduct.service';

describe('SellinghasproductService', () => {
  let service: SellinghasproductService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SellinghasproductService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
