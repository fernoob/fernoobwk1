import { Component } from '@angular/core';

import { Customer } from '../../../models/customer';
import { CustomerService } from '../../../services/customer.service';

import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  customers: Customer[];

  constructor(
    private customerService: CustomerService,
    private router: Router
  ) {  }

  ngOnInit() {
    this.getCustomers();
  }

  // Chama o serviço para obtém todos os customers
  getCustomers() {
    this.customerService.getCustomers().subscribe((customers: Customer[]) => {
      this.customers = customers;
    });
  }

  deteleItem(customer: Customer) {
    this.customerService.deleteCustomer(customer).subscribe(() => {
      this.getCustomers();
    });
  }
}
