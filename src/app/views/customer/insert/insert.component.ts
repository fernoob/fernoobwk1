import { Component } from '@angular/core';

import { Customer } from '../../../models/customer';
import { CustomerService } from '../../../services/customer.service';

import { Router } from "@angular/router";

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css'],
})
export class InsertComponent {

  customer: Customer = {
    customer: '',
    cid: '',
    cpf: '',
    birthday: '',
    email: '',
    address: '',
    address2: '',
    cep: '',
    number: '',
    city: '',
  };

  submitted = false;

  constructor(
    private customerService: CustomerService,
    private router: Router
  ) {}

  saveCustomer(): void {
   const data: Customer = {
     customer: this.customer.customer,
     cid: this.customer.cid,
     cpf: this.customer.cpf,
     birthday: this.customer.birthday,
     email: this.customer.email,
     address: this.customer.address,
     address2: this.customer.address2,
     cep: this.customer.cep,
     number: this.customer.number,
     city: this.customer.city,
   };

   this.customerService.saveCustomer(data)
     .subscribe({
       next: (res) => {
         //console.log(res);
         this.submitted = true;
       },
       error: (e) => console.error(e)
     });
   }

}
