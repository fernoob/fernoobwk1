import { Component } from '@angular/core';

import { Customer } from '../../../models/customer';
import { CustomerService } from '../../../services/customer.service';

import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent {

  customer: Customer = {
    customer: "",
    cid: "",
    cpf: "",
    address: "",
    address2: "",
    cep: "",
    number: "",
    city: "",
  }

  submitted=false;

  constructor(
    private customerService: CustomerService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.getCustomerById(this.route.snapshot.paramMap.get('id_customer'));
  }

  getCustomerById(id_customer: any) {
    this.customerService.getCustomerById(id_customer).subscribe((customer: Customer) => {
      this.customer=customer;
    })
  }

  updateCustomer(): void {
   const data: Customer = {
     id_customer: this.customer.id_customer,
     customer: this.customer.customer,
     cid: this.customer.cid,
     cpf: this.customer.cpf,
     address: this.customer.address,
     address2: this.customer.address2,
     cep: this.customer.cep,
     number: this.customer.number,
     city: this.customer.city,
   };

   this.customerService.updateCustomer(data)
     .subscribe({
       next: (res) => {
         //console.log(res);
         this.submitted = true;
         this.router.navigate(['/customers/']);
       },
       error: (e) => console.error(e)
     });
   }

}
