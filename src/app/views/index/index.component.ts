import { Component } from '@angular/core';

import { Selling } from '../../models/selling';
import { SellingService } from '../../services/selling.service';

import { Product } from '../../models/product';
import { ProductService } from '../../services/product.service';

import { Customer } from '../../models/customer';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent {

  sellings: Selling[] = [];
  totalsellings = 0;

  products: Product[] = [];
  totalproducts = 0;

  customers: Customer[] = [];
  totalcustomers = 0;

  constructor(
    private customerService: CustomerService,
    private productService: ProductService,
    private sellingService: SellingService,
  ) {}

  ngOnInit() {
    this.getCustomers();
    this.getProducts();
    this.getSellings();
  }

  // Chama o serviço para obtém todos os customers
  getCustomers() {
    this.customerService.getCustomers().subscribe((customers: Customer[]) => {
      this.customers = customers;
      this.totalcustomers = customers.length;
    });
  }

  // Chama o serviço para obtém todos os customers
  getProducts() {
    this.productService.getProducts().subscribe((products: Product[]) => {
      this.products = products;
      this.totalproducts = products.length;
    });
  }

  // Chama o serviço para obtém todos os customers
  getSellings() {
    this.sellingService.getSellings().subscribe((sellings: Selling[]) => {
      this.sellings = sellings;
      this.totalsellings = sellings.length;
    });
  }

}
