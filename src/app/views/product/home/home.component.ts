import { Component } from '@angular/core';

import { Product } from '../../../models/product';
import { ProductService } from '../../../services/product.service';

import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  products: Product[];

  constructor(
    private productService: ProductService,
    private router: Router
  ) {  }

  ngOnInit() {
    this.getProducts();
  }

  // Chama o serviço para obtém todos os customers
  getProducts() {
    this.productService.getProducts().subscribe((products: Product[]) => {
      this.products = products;
    });
  }

  deteleItem(product: Product) {
    this.productService.deleteProduct(product).subscribe(() => {
      this.getProducts();
    });
  }
}
