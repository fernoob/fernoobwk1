import { Component } from '@angular/core';
import { Product } from '../../../models/product';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css']
})
export class InsertComponent {

  product: Product = {
    product: "",
    price: 0,
  }
  submitted = false;

  constructor( private productService : ProductService ) { };

  saveProduct(): void {
   const data: Product = {
     product: this.product.product,
     price: this.product.price
   };

   this.productService.saveProduct(data)
     .subscribe({
       next: (res) => {
         //console.log(res);
         this.submitted = true;
       },
       error: (e) => console.error(e)
     });
   }

 newProduct(): void {
   this.submitted = false;
   this.product = {
     product: '',
     price: 0,
   };
 }
}
