import { Component } from '@angular/core';
import { Product } from '../../../models/product';
import { ProductService } from '../../../services/product.service';

import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent {

  product: Product = {
    product: "",
    pid: "",
    price: 0,
  }
  submitted=false;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router
  ){}

  ngOnInit() {
    this.getProductById(this.route.snapshot.paramMap.get('id_product'));
  }

  getProductById(id_product: any) {
    this.productService.getProductById(id_product).subscribe((product: Product) => {
      this.product=product;
    })
  }

  updateProduct(): void {
   const data: Product = {
     id_product: this.product.id_product,
     product: this.product.product,
     pid: this.product.pid,
     price: this.product.price
   };

   this.productService.updateProduct(data)
     .subscribe({
       next: (res) => {
         //console.log(res);
         this.submitted = true;
         this.router.navigate(['/products/']);
       },
       error: (e) => console.error(e)
     });
   }

}
