import { Component } from '@angular/core';

import { Customer } from '../../../models/customer';
import { CustomerService } from '../../../services/customer.service';

import { Product } from '../../../models/product';
import { ProductService } from '../../../services/product.service';

import { Selling } from '../../../models/selling';
import { SellingService } from '../../../services/selling.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent {

  customers: Customer[] = [];
  // products: Product[] = [];
  selling: Selling = {};
  submitted=false;

  constructor(
    // private productService: ProductService,
    private customerService: CustomerService,
    private sellingService: SellingService,
  ) {}

  ngOnInit() {
    this.getCustomers();
    // this.getProducts();
  }

  // Chama o serviço para obtém todos os customers
  getCustomers() {
    this.customerService.getCustomers().subscribe((customers: Customer[]) => {
      this.customers = customers;
    });
  }

  // Chama o serviço para obtém todos os customers
  // getProducts() {
  //   this.productService.getProducts().subscribe((products: Product[]) => {
  //     this.products = products;
  //   });
  // }

  createSelling() {
    const data: Selling = {
      id_customer: this.selling.id_customer,
      date: '20200901',
    };

    this.sellingService.saveSelling(data)
    .subscribe({
      next: (res) => {
        //console.log(res);
        this.submitted = true;
      },
      error: (e) => console.error(e)
    });
  }
}
