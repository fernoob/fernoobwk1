import { Component } from '@angular/core';

import { Router } from "@angular/router";

import { Selling } from '../../../models/selling';
import { SellingService } from '../../../services/selling.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  sellings: Selling[];

  constructor(
    private sellingService: SellingService,
    private router: Router
  ) {  }

  ngOnInit() {
    this.getSellings();
  }

  // Chama o serviço para obtém todos os customers
  getSellings() {
    this.sellingService.getSellings().subscribe((sellings: Selling[]) => {
      this.sellings = sellings;
    });
  }

  deteleItem(selling: Selling) {
    this.sellingService.deleteSelling(selling).subscribe(() => {
      this.getSellings();
    });
  }
}
